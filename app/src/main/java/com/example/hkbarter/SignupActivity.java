package com.example.hkbarter;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class SignupActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private EditText input_account;
    private EditText input_password;
    private EditText input_name;
    private Spinner input_district;
    private Button button_signup;
    private FirebaseUser user;
    private DatabaseReference mDatabase_user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        initView();
    }

    private void initView() {

        mAuth = FirebaseAuth.getInstance();
        mDatabase_user = FirebaseDatabase.getInstance().getReference("Users");
        input_name = findViewById(R.id.name);
        input_district = findViewById(R.id.spinner_district);
        input_account = findViewById(R.id.email);
        input_password = findViewById(R.id.password);
        button_signup = findViewById(R.id.button_submit);

        button_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String name = input_name.getText().toString();
                final String district = input_district.getSelectedItem().toString();
                final String account = input_account.getText().toString();
                String password = input_password.getText().toString();
                if(TextUtils.isEmpty(account)){
                    Toast.makeText(getApplicationContext(), "Please enter email address!\n請輸入帳戶名稱!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(TextUtils.isEmpty(password)){
                    Toast.makeText(getApplicationContext(), "Please password!\n請輸入密碼!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (password.length() < 8) {
                    Toast.makeText(getApplicationContext(), "Password too short, enter minimum 8 characters!\n密碼太短，最少8位!", Toast.LENGTH_SHORT).show();
                    return;
                }

                mAuth.createUserWithEmailAndPassword(account, password)
                        .addOnCompleteListener(SignupActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {

                                    String user_id = mAuth.getCurrentUser().getUid();
                                    DatabaseReference current_user = mDatabase_user.child(user_id);
                                    current_user.child("Name").setValue(name);
                                    current_user.child("District").setValue(district);
                                    // Reference: https://stackoverflow.com/questions/39191403/create-new-user-with-names-username-etc-in-firebase

                                    Toast.makeText(SignupActivity.this, "Sign up successfully!\n註冊成功!", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent();
                                    intent.setClass(SignupActivity.this, HomeActivity.class);
                                    startActivity(intent);
                                    finish();
                                } else {
                                    Toast.makeText(SignupActivity.this, "Sign up fails!\n註冊失敗!", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
            }



        });
    }

}

// Reference: https://givemepass.blogspot.com/2017/04/firebase-authentication.html?m=1
